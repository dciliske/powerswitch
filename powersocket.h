#ifndef  _POWERSOCKET_H_INC
#define  _POWERSOCKET_H_INC

#include <basictypes.h>
#include <pins.h>

#define SOCKET_LABEL_MAX 256

struct ActivityRecord
{
    enum methodTypes{
        web = 1,
        userScript,
        autoping,
        remote
    };
    uint32_t tick; /* Time tick when this outlet last changed state */
    uint16_t UID; /* The UID of the user to last change the state */
    uint8_t method; /* The method used to send the control command */
};

class PowerSocket
{
    private:
        uint8_t pinnum; /* The actual pin that needs to be toggled */
        bool state; /* 0 = off, 1 = on */
        bool critical; /* Should the socket be treated as critical when changing settings? */
        PinIO *pin; /* The actual pin that needs to be toggled */
        char label[SOCKET_LABEL_MAX+1]; /* Label to descibe this socket */
        ActivityRecord lastChange; /* Record of the last change for this socket */

    public:
        PowerSocket( char *socketLabel, bool startState, bool socketCritical, 
                        uint8_t socketPinnum );
        void switchOff();
        void switchOn();
        void cycle( uint32_t delay );
        bool getState() const;
        bool getCritical() const;
        void setCritical( bool value );
        void setLabel( char *newLabel );
        uint16_t getLabelLength() const;
        const char * getLabel() const;
        ActivityRecord getLastChange() const;

        /* serialize the class onto the file descriptor, in the chosen format */
        int32_t serialize( int fd ); 
        /* deserialize the class from the file descriptor, from the chosen format */
        bool deserialize( int fd );
};

#endif   /* ----- #ifndef _POWERSOCKET_H_INC  ----- */
