| Pin Number  | Function                 |
| ----------- | ---------                |
| 9           | IRQ7/GPIO                |
| 10          | UART2 CTS/UART6 TX/GPIO  |
| 12          | GPIO                     |
| 13          | UART 2 RX                |
| 14          | UART2 RTS/UART6 RX/GPIO  |
| 15          | SDHC DATA3/SPI1 CS0/GPIO |
| 16          | UART2 TX                 |
| 17          | VCC                      |
| 18          | GND                      |
| 19          | GPIO                     |
| 20          | UART8 RX                 |
| 21          | SDHC DATA1/GPIO          |
| 22          | UART9 TX                 |
| 23          | SDHC DATA2/GPIO          |
| 24          | UART0 RX                 |
| 25          | GPIO                     |
| 26          | UART0 TX                 |
| 27          | UART8 TX                 |
| 28          | UART0 RTS/UART4 RX/GPIO  |
| 29          | UART8 RX                 |
| 30          | UART0 CTS/UART4 TX/GPIO  |
| 31          | SDHC CLK/SPI1 CLK        |
| 32          | UART1 RX                 |
| 33          | SDHC CMD/SPI1 SIN        |
| 34          | UART1 TX                 |
| 35          | SDHC DATA0/SPI1 SOUT     |
| 36          | UART1 RTS/UART5 RX/GPIO  |
| 37          | SDHC DATA1/SPI1 CS1      |
| 38          | UART1 CTS/UART5 TX/GPIO  |
| 39          | SDHC DATA2/SPI1 CS2      |
| 40          | SPEEDLED                 |
| 41          | VCC3V ETH                |
| 42          | LINKLED                  |
| 43          | ETH RX+                  |
| 44          | ETH RX-                  |
| 45          | ETH TX+                  |
| 46          | ETH TX-                  |
| 47          | VCC3V                    |
| 48          | GND                      |
| 49          | IRQ3/GPIO                |
| 50          | IRQ2/GPIO                |

