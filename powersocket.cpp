#include <basictypes.h>
#include <pins.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iosys.h>
#include <ucos.h>
#include <constants.h>
#include "powersocket.h"

PowerSocket::PowerSocket( 
        char *socketLabel, 
        bool startState, 
        bool socketCritical, 
        uint8_t socketPinnum )
            : pinnum(socketPinnum),
            state(startState), 
            critical(socketCritical),
            pin(NULL)
{
    pin = (PinIO *)malloc(sizeof(PinIO));
    memcpy( pin, &(Pins[pinnum]), sizeof(PinIO) );
    pin->function(0);
    *pin = state;
    int len = strlen(socketLabel);
    strncpy(label, socketLabel, (len <= SOCKET_LABEL_MAX-1) ? len : SOCKET_LABEL_MAX-1);
    if (len > SOCKET_LABEL_MAX-1)
    {
        label[SOCKET_LABEL_MAX-1] = '\0';
    }
    else
    {
        label[strlen(socketLabel)] = '\0';
    }
    memset((void *)&lastChange, 0, sizeof(ActivityRecord));
}

void PowerSocket::switchOff ( )
{
    state = false;
    *pin = 0;
}		/* -----  end of method PowerSocket::switchOff  ----- */


/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  switchOn
 *--------------------------------------------------------------------------------------
 */
void PowerSocket::switchOn ( )
{
    state = true;
    *pin = 1;
}		/* -----  end of method PowerSocket::switchOn  ----- */


/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  cycle
 *--------------------------------------------------------------------------------------
 */
void PowerSocket::cycle ( uint32_t delay )
{
    state = false;
    *pin = 0;
    OSTimeDly(0.5 * TICKS_PER_SECOND);
    state = true;
    *pin = 1;
    // schedule( CurrentTick + delay, 1 );
    return ;
}		/* -----  end of method PowerSocket::cycle  ----- */


/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  get_state
 *--------------------------------------------------------------------------------------
 */
bool PowerSocket::getState (  ) const
{
    return state;
}		/* -----  end of method PowerSocket::get_state  ----- */

/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  get_Critical
 *--------------------------------------------------------------------------------------
 */
bool PowerSocket::getCritical() const
{
    return critical;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  set_Critical
 *--------------------------------------------------------------------------------------
 */
void PowerSocket::setCritical(bool value)
{
    critical = value;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  setLabel
 *--------------------------------------------------------------------------------------
 */
void PowerSocket::setLabel (char *newLabel )
{
    int len = strlen(newLabel);
    len = (len <= SOCKET_LABEL_MAX) ? len : SOCKET_LABEL_MAX;
    strncpy(label, newLabel, len);
    label[len] = '\0';
}		/* -----  end of method PowerSocket::setLabel  ----- */


/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  getLabelLength
 *--------------------------------------------------------------------------------------
 */
uint16_t PowerSocket::getLabelLength ( ) const
{
    return strlen(label);
}		/* -----  end of method PowerSocket::getLabelLength  ----- */


/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  getLabel
 *--------------------------------------------------------------------------------------
 */
const char * PowerSocket::getLabel ( ) const
{
    return label;
}		/* -----  end of method PowerSocket::getLabel  ----- */


/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  getLastChangeTick
 *--------------------------------------------------------------------------------------
 */

ActivityRecord PowerSocket::getLastChange ( ) const
{
    return lastChange;
}		/* -----  end of method PowerSocket::getLastChangeTick  ----- */


/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  serialize
 *--------------------------------------------------------------------------------------
 */
int32_t PowerSocket::serialize ( int fd )
{
    /* invalid file descriptor, return error */
    if (fd < 0) return -1;

    // 5 - bufferSize
    // 5 - pinnum
    // 3 - state
    // 3 - critical
    // 12 - tick
    // 7 - UID
    // 5 - method
    // + 1
    char writeBuffer[SOCKET_LABEL_MAX + 41]; 

    siprintf(writeBuffer+5, "%u, %u, %u, %lu, %u, %u, %s",
            pinnum, state, critical, lastChange.tick, lastChange.UID,
            lastChange.method, label);
    int len, remaining;
    int digits = 0;
    remaining = len = strlen(writeBuffer);
    for ( ; remaining > 0; remaining /= 10, digits++);
    for ( int i = 0; i < 3 - digits; i++)
    {
        memset(writeBuffer+i, (int)'0', 1);
    }
    siprintf(writeBuffer+(3-digits), "%u,", remaining);
    memset(writeBuffer+4, (int)' ', 1);
    int written;
    int attempts = 0;
    while ( ((written = write(fd, writeBuffer, remaining)) < remaining)
            && (written != -1) && (attempts < 5) )
    {
        if (written == 0 && remaining > 0)
        {
            attempts++;
            OSTimeDly(3);
        }
        remaining -= written;
    }
    if (remaining || written == -1)
        return -1;
    return strlen(writeBuffer);
}		/* -----  end of method PowerSocket::serialize  ----- */


/*
 *--------------------------------------------------------------------------------------
 *       Class:  PowerSocket
 *      Method:  deserialize
 *--------------------------------------------------------------------------------------
 */
bool PowerSocket::deserialize ( int fd )
{
    int bytesRead;
    uint8_t tempPin;
    bool tempState;
    bool tempCrit;
    uint32_t tick;
    uint16_t UID;
    uint8_t method;
    char tempLabel[SOCKET_LABEL_MAX];
    char readBuffer[SOCKET_LABEL_MAX + 41];
    int len;

    bytesRead = read(fd, readBuffer, 3);
    readBuffer[3] = '\0';
    if (bytesRead < 3) return false;
    len = atoi(readBuffer);
    bytesRead = read(fd, readBuffer, 2+len);
    if (bytesRead < 2+len) return false;
    bytesRead = scanf(readBuffer, ", %u, %u, %u, %lu, %u, %u, %s",
                    &tempPin, &tempState, &tempCrit, &tick,
                    &UID, &method, tempLabel);
    if (bytesRead < 7) return false;
    memcpy( pin, &(Pins[tempPin]), sizeof(PinIO) );
    pinnum = tempPin;
    state = tempState;
    critical = tempCrit;
    strncpy( label, tempLabel, SOCKET_LABEL_MAX );
    lastChange.tick = tick;
    lastChange.UID = UID;
    lastChange.method = method;

    return true;
}		/* -----  end of method PowerSocket::deserialize  ----- */


