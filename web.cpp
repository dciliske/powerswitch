/* NB Library Definitions */
#include "predef.h"

/* C Standard Library */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Portability & uCos Definitions */
#include <basictypes.h>
#include <ucos.h>
#include <utils.h>

/* NB Runtime Libraries */
#include <constants.h>
#include <system.h>

/* NB I/O System */
#include <iosys.h>

/* NB Network Stack */
#include <nettypes.h>
#include <buffers.h>
#include <counters.h>

/* NB TCP/IP */
#include <arp.h>
#include <ip.h>
#include <tcp.h>

/* NB HTTP */
#include <http.h>
#include <htmlfiles.h>

#include "app_defs.h"


http_posthandler *oldPostHandler;

static int setOutletStates( int sock, char *pData )
{
    int charCount;
    char buf[100];
    int outletNumber;
    bool allOutlets = false;
    int action;
    if ( (charCount = ExtractPostData("outletNumber", pData, buf, 100)) > 0 )
    {
        if (stricmp(buf, "all") == 0)
        {
            allOutlets = true;
            outletNumber = -1;
        }
        else
        {
            outletNumber = atoi(buf);
        }
    }
    else
    {
        outletNumber = -1;
    }

    if ( (charCount= ExtractPostData("action", pData, buf, 100)) > 0 )
    {
        action = atoi(buf);
    }
    else
    {
        action = -1;
    }

    if( (outletNumber > 0) && (outletNumber <= DEVICE_SOCKET_COUNT) && (action >= 0) )
    {
        PowerSocket *outlet = powersockets[outletNumber - 1];
        switch(action)
        {
            case outletSwitchAction::On:
                outlet->switchOn();
                break;
            case outletSwitchAction::Off:
                outlet->switchOff();
                break;
            case outletSwitchAction::Cycle:
                outlet->cycle(TICKS_PER_SECOND);
                break;
            default:
                break;
        }
    }
    else if ( allOutlets && (action >= 0) )
    {
        int delay = (action == outletSwitchAction::Off) ? 0 : 0.5 * TICKS_PER_SECOND;
        for (int i = 0; i < DEVICE_SOCKET_COUNT; i++)
        {
            PowerSocket *outlet = powersockets[i];
            switch(action)
            {
                case outletSwitchAction::On:
                    outlet->switchOn();
                    break;
                case outletSwitchAction::Off:
                    outlet->switchOff();
                    break;
                case outletSwitchAction::Cycle:
                    outlet->cycle(TICKS_PER_SECOND);
                    break;
                default:
                    break;
            }
            if(delay)
                OSTimeDly(delay);
        }
    }
    RedirectResponse( sock, "/control/outlets.html" );

    return 1;

}

extern char serverName[];

static int processSetupAndNaming( int sock, char *pData )
{
    int charCount;
    char buf[SOCKET_LABEL_MAX+1];
    char inputName[16];
    bool checked;

    if ( (charCount = ExtractPostData("unitName", pData, buf, SOCKET_LABEL_MAX)) > 0 )
    {
        strcpy(serverName, buf);
    }
    for ( int i = 0; i < DEVICE_SOCKET_COUNT; i++ )
    {
        siprintf( inputName, "outlet%d", i+1 );
        if ( (charCount = ExtractPostData(inputName, pData, buf, SOCKET_LABEL_MAX)) > 0 )
        {
            powersockets[i]->setLabel( buf );
        }

        siprintf( inputName, "critical%d", i+1);
        if ( (charCount = ExtractPostData(inputName, pData, buf, SOCKET_LABEL_MAX)) > 0 )
        {
            checked = atoi(buf);
        }
        else
        {
            checked = false;
        }
        powersockets[i]->setCritical(checked);
    }
    
    RedirectResponse( sock, "/admin/setup.html" );
    return 1;
}

int MyDoPost( int sock, char *url, char *pData, char *http_rxBuffer )
{
    if (httpstricmp( url, "/CONTROL/SETOUTLET.CGI"))
    {
        return setOutletStates( sock, pData );
    }
    if (httpstricmp( url, "/ADMIN/NEWSETTINGS.CGI"))
    {
        return processSetupAndNaming( sock, pData );
    }

    if (oldPostHandler)
    {
        return oldPostHandler(sock, url, pData, http_rxBuffer);
    }
    return 0;
}

void InitCustomHandlers()
{
    oldPostHandler = SetNewPostHandler( MyDoPost );
}
