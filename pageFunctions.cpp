#include "predef.h" 
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <startnet.h>
#include <autoupdate.h>
#include <dhcpclient.h>

#include "app_defs.h"
#include "powersocket.h"

const char socketStateOff[] = "            <td><h3 class=\"alert alert-error\">OFF</h3></td>";
const char socketStateOn[] = "            <td><h3 class=\"alert alert-success\">ON</h3></td>";

const char disabled[] = " disabled";
const char none[] = "";

const char *socketButtons[] = {"            <td>\r\n" 
        "                <div class=\"btn-group\"><button " 
        "class=\"btn btn-success %s\"", 

        " id=\"button%d", 

        "_%d", 

        "\" onclick=\"setAndSendOutletConfig(%d", 

        ", %d);\">"
        "                    <i class=\"icon-arrow-up\"></i>Switch On</button>", 

        "                <button class=\"btn btn-danger%s", 

        "\" id=\"button%d", 

        "_%d\" ", 

        "onclick=\"setAndSendOutletConfig(%d", 

        ", %d);\">"
        "                    <i class=\"icon-arrow-down\"></i>Switch Off</button>", 

        "                <button class=\"btn btn-warning%s", 

        "\" id=\"button%d_", 

        "%d\" ", 

        "onclick=\"setAndSendOutletConfig(%d", 

        ", %d);\">"
        "                    <i class=\"icon-refresh\"></i>Cycle Power</button>"
        "                </div>"
        "            </td>\r\n" 
        "        </tr>\r\n"};

void writePowerSocketTableRows( int fd, const char *url )
{
    char buffer[SOCKET_LABEL_MAX+256];
    int written;
    const char *a;
    const char *b;
    for (int i = 0; i < DEVICE_SOCKET_COUNT; i++)
    { 
        written = siprintf(buffer, "        <tr id=\"OutletRow%d\">\r\n"
                                 "            <td>%d</td>\r\n"
                                 "            <td>%s</td>\r\n",
                                i+1, i+1, powersockets[i]->getLabel()
                            );
        writeall( fd, buffer, written );

        if (powersockets[i]->getState())
        {
            a = disabled;
            b = none;
            writeall( fd, socketStateOn, strlen(socketStateOn) );

//                                disabled, i+1, outletSwitchAction::On,
//                                i+1, outletSwitchAction::On,
//                                none, i+1, outletSwitchAction::Off,
//                                i+1, outletSwitchAction::Off,
//                                none, i+1, outletSwitchAction::Cycle,
//                                i+1, outletSwitchAction::Cycle
//                                );
//            writeall( fd, buffer, written );
        }
        else
        {
            a = none;
            b = disabled;
            writeall( fd, socketStateOff, strlen(socketStateOff) );

//            written = siprintf(buffer, socketButtons, 
//                                none, i+1, outletSwitchAction::On,
//                                i+1, outletSwitchAction::On,
//                                disabled, i+1, outletSwitchAction::Off,
//                                i+1, outletSwitchAction::Off,
//                                disabled, i+1, outletSwitchAction::Cycle,
//                                i+1, outletSwitchAction::Cycle
//                                );
//            writeall( fd, buffer, written );
        }
        written = siprintf(buffer, socketButtons[0], a); 
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[1], i+1);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[2], outletSwitchAction::On);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[3], i+1);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[4], outletSwitchAction::On);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[5], b);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[6], i+1);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[7], outletSwitchAction::Off);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[8], i+1);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[9], outletSwitchAction::Off);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[10], b);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[11], i+1);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[12], outletSwitchAction::Cycle);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[13], i+1);
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketButtons[14], outletSwitchAction::Cycle);
        writeall( fd, buffer, written );
    }
}

const char *socketLabel[] = {
"\t\t\t\t\t\t\t<tr>\r\n"
"\t\t\t\t\t\t\t\t<td>Outlet 1</td>\r\n"
"\t\t\t\t\t\t\t\t<td><input type=\"text\" class=\"input-large\" id=\"outlet%d",

"\" name=\"outlet%d\" value=\"%s\"></td>\r\n",

"\t\t\t\t\t\t\t\t<td><input type=\"checkbox\" id=\"critical%d",

"\" name=\"critical%d\"",

"%s></td>\r\n"
"\t\t\t\t\t\t\t\t</tr>\r\n"
};

const char checked[] = " checked";

void writeNamingSetupRows( int fd, const char * url )
{
    char buffer[SOCKET_LABEL_MAX+256];
    int written;
    for (int i = 0; i < DEVICE_SOCKET_COUNT; i++)
    { 
        written = siprintf(buffer, socketLabel[0], i+1); 
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketLabel[1], i+1, powersockets[i]->getLabel());
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketLabel[2], i+1); 
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketLabel[3], i+1); 
        writeall( fd, buffer, written );
        written = siprintf(buffer, socketLabel[4], (powersockets[i]->getCritical()) ? checked : none); 
        writeall( fd, buffer, written );
        
    }
}
