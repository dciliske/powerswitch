#ifndef  _APP_DEFS_H_INC
#define  _APP_DEFS_H_INC

#include "powersocket.h"

#define DEVICE_SOCKET_COUNT 8

struct outletSwitchAction
{
    enum 
    {
        On,
        Off,
        Cycle
    };
};

extern PowerSocket *powersockets[];

void InitCustomHandlers();

#endif   /* ----- #ifndef _APP_DEFS_H_INC  ----- */
