#include "predef.h" 
#include <stdio.h>
#include <ctype.h>
#include <startnet.h>
#include <autoupdate.h>
#include <dhcpclient.h>

#include "app_defs.h"
#include "powersocket.h"

#define DEVICE_SOCKET_COUNT 8
#define SERVER_NAME_MAX_SIZE 256

extern "C" { 
void UserMain(void * pd);
}

PowerSocket *powersockets[DEVICE_SOCKET_COUNT];
//static uint8_t pinList[] = { 9, 12, 19, 21, 23, 25, 49, 50 };
static uint8_t pinList[] = { 19, 21, 23, 25, 23, 25, 49, 50 };

const char * AppName="PowerSwitchApp";
char serverName[SERVER_NAME_MAX_SIZE];

void setupSockets()
{
    char base[] = "Outlet";
    char buffer[12];
    for ( int i = 0; i < DEVICE_SOCKET_COUNT; i++ )
    {
        siprintf(buffer, "%s %d", base, i+1);
        powersockets[i] = new PowerSocket( buffer, true, false, pinList[i] );
    }
    powersockets[0]->switchOff();
}

void UserMain(void * pd)
{
    InitializeStack();/* Setup the TCP/IP stack buffers etc.. */     

    GetDHCPAddressIfNecessary();/*Get a DHCP address if needed*/
    /*You may want to add a check for the return value from this function*/
    /*See the function definition in  \nburn\include\dhcpclient.h*/

    OSChangePrio(MAIN_PRIO);/* Change our priority from highest to something in the middle */

    EnableAutoUpdate();/* Enable the ability to update code over the network */ 

    StartHTTP();/* Start the web serrver */


    iprintf("Application started\n");
    siprintf(serverName, "NAME GOES HERE");

    setupSockets();
    InitCustomHandlers();
    while (1)
    {
        OSTimeDly(TICKS_PER_SECOND * 1);
    }
}
